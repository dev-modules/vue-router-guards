# Vue router guards
package for simplify working with guard in vue router

* Install
```
npm i -S vue-router-guards
```
* Initialize pipeline
```javascript
import { Pipeline } from "vue-router-guards";

const pipeline = new Pipeline();
```

* Guard example
```javascript

import { GuardResult } from "vue-router-guards";

class AuthGuard {
    execute(to, from) {
        if(user.isAuth()) {
            return new GuardResult(true);
        }
        
        return new GuardResult(false, "/login");
    }
}
```

* Use guard
```javascript
pipeline.use(new AuthGuard());
```

* Pass execute method of pipeline in router hook
```javascript
router.beforeEach((to, from, next) => pipeline.execute(to, from, next));
```
or
```javascript
router.beforeEach(pipeline.execute.bind(pipeline));

```

* Nuances
1. Guards executes in order
2. On first failure subsequent guards will not be executed
