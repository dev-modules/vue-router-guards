/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class GuardResult
 */
var GuardResult = /** @class */ (function () {
    function GuardResult(passed, failureRoute) {
        if (passed === void 0) { passed = true; }
        if (failureRoute === void 0) { failureRoute = "/"; }
        this.passed = passed;
        this.failureRoute = failureRoute;
    }
    return GuardResult;
}());
export { GuardResult };
//# sourceMappingURL=GuardResult.js.map