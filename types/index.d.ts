import { Route } from "vue-router";
import { GuardResult } from "../src/GuardResult";

export * from "../src/GuardResult";
export * from "../src/Pipeline";


export interface Guard {
    execute(to: Route, from: Route): Promise<GuardResult>;
}


