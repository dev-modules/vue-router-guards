/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Pipeline
 */
import { Route } from "vue-router";
import { RawLocation } from "vue-router/types/router";
import { Guard } from "../types";

export class Pipeline {
    private readonly guards: Guard[] = [];

    constructor() {
        this.execute.bind(this);
        this.use.bind(this);
    }

    public use(guard: Guard | object | any) {
        this.guards.push(guard as Guard);
    }

    public async execute(to: Route, from: Route, next: (to?: RawLocation) => void): Promise<void> {
        for(const guard of this.guards) {
            const guardResult = await guard.execute(to, from);
            if(!guardResult.passed) {
                next(guardResult.failureRoute);
                return;
            }
        }

        next();
    }
}
