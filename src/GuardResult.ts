/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class GuardResult
 */
export class GuardResult {
    constructor(
        public readonly passed: boolean = true,
        public readonly failureRoute: string = "/"
    ) {}
}
